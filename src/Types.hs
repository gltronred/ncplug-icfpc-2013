{-# LANGUAGE OverloadedStrings #-}

module Types where

import Control.Monad (mzero)
import Data.Aeson
import Data.ByteString.Char8 as S
import qualified Data.Map as M (fromList, lookup)
import Data.Maybe (isJust, fromJust)
import Data.Text as T
import Data.Word

type Id = S.ByteString -- id задачи
type Label = Int -- имя переменной
type Point = Word64 -- аргумент
type Val = Word64 -- значение, для удобства
type OpConstraint = [Operator]

type Solver = OpConstraint -> [(Point,Val)] -> [Program]

data Problem = Problem { problemId :: Id
                       , problemSize :: Int
                       , problemOperators :: [Operator]
                       , problemSolved :: Maybe Bool
                       , problemTime :: Maybe Double } deriving (Eq, Show)

data Operator = O1 Operator1 | O2 Operator2 | If0 | Tfold| Fold | Bonus deriving (Eq,Show,Read,Ord)

data Operator1 = Not | Shl1 | Shr1 | Shr4 | Shr16 deriving (Eq,Read,Ord,Bounded,Enum)

data Operator2 = And | Or | Xor | Plus deriving (Eq,Read,Ord,Bounded, Enum)

data Program = Lambda1 Label Expression deriving (Eq,Ord)

data Training = Training { trainingChallenge :: S.ByteString
                         , trainingId :: Id
                         , trainingSize :: Int
                         , trainingOperators :: OpConstraint } deriving (Eq,Show)

data Window = Window { windowResetsIn :: Double
                     , windowAmount :: Int
                     , windowLimit :: Int } deriving (Eq, Show)

data Status = Status { statusEasyChairId :: String
                     , statusContestScore :: Double
                     , statusLightningScore :: Double
                     , statusTrainingScore :: Double
                     , statusMismatches :: Int
                     , statusNumRequests :: Int
                     , statusRequestWindow :: Window
                     , statusCpuWindow :: Window
                     , statusCpuTotalTime :: Double } deriving (Eq,Show)

data Expression = Zero | One | Id Label
                | IfE0 Expression Expression Expression
                | FoldE Expression Expression Lambda2
                | Op1 Operator1 Expression
                | Op2 Operator2 Expression Expression deriving (Eq, Ord)

data Lambda2 = Lambda2 Label Label Expression deriving (Eq, Ord)

instance Show Expression where
  show Zero = "0"
  show One  = "1"
  show (Id id) = "x_" ++ show id
  show (IfE0 c t f) = "(if0 " ++ (show c) ++ " " ++ (show t) ++ " " ++ (show f) ++ ")"
  show (FoldE l i lambda) = "(fold " ++ (show l) ++ " " ++ (show i) ++ " " ++ (show lambda) ++ ")"
  show (Op1 op e) = "(" ++ (show op) ++ " " ++ (show e) ++ ")"
  show (Op2 op e1 e2) = "(" ++ (show op) ++ " " ++ (show e1) ++ " " ++ (show e2) ++ ")"

instance Show Lambda2 where
  show (Lambda2 id1 id2 e) = "(lambda (x_" ++ show id1 ++ " x_" ++ show id2 ++ ")" ++ " " ++ (show e) ++ ")"

instance Show Program where
  show (Lambda1 id e) = "(lambda (x_" ++ show id ++ ")" ++ " " ++ (show e) ++ ")"

instance Show Operator1 where
  show Not   = "not"
  show Shl1  = "shl1"
  show Shr1  = "shr1"
  show Shr4  = "shr4"
  show Shr16 = "shr16"

instance Show Operator2 where
  show And  = "and"
  show Or   = "or"
  show Xor  = "xor"
  show Plus = "plus"

instance ToJSON Program where
  toJSON (Lambda1 id e) = String $ T.pack $ "(lambda (x_" ++ show id ++ ")" ++ " " ++ (show e) ++ ")"

jsonToOp = M.fromList [("not",O1 Not), ("shl1",O1 Shl1), ("shr1",O1 Shr1), ("shr4",O1 Shr4), ("shr16",O1 Shr16)
                      ,("and",O2 And), ("or",O2 Or), ("xor",O2 Xor), ("plus",O2 Plus)
                      ,("if0",If0), ("tfold",Tfold), ("fold",Fold), ("bonus",Bonus)]

instance FromJSON Operator where
  parseJSON (String s) = if isJust v then return (fromJust v) else mzero where v = M.lookup s jsonToOp
  parseJSON _ = mzero

instance ToJSON Operator where
  toJSON (O1 op) = String $ T.pack $ show op
  toJSON (O2 op) = String $ T.pack $ show op
  toJSON If0 = String "if0"
  toJSON Tfold = String "tfold"
  toJSON Fold = String "fold"
  toJSON Bonus = String "bonus"  -- as long as we have tfold


class Sizeable a where
  size :: a -> Int

instance Sizeable Expression where
  size Zero = 1
  size One = 1
  size (Id _) = 1
  size (IfE0 c t f) = 1 + size c + size t + size f
  size (FoldE l i (Lambda2 _ _ e)) = 2 + size l + size i + size e
  size (Op1 op e) = 1 + size e
  size (Op2 op e1 e2) = 1 + size e1 + size e2

instance Sizeable Program where
  size (Lambda1 _ e) = 1 + size e

