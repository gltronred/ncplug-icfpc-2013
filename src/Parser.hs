module Parser where

import Types
import Text.ParserCombinators.Parsec hiding ((<|>))
import Control.Applicative

loadProg :: String -> Either ParseError Program
loadProg = parse prog ""

prog :: Parser Program
prog = do
  string "(lambda ("
  i1 <- idstring
  string ") "
  e <- expr
  char ')'
  return $ Lambda1 i1 e

expr :: Parser Expression
expr = (const Zero <$> char '0') <|>
       (const One <$> char '1') <|> (Id <$> try idstring) <|>
       try ifexpr <|> try foldexpr <|> try op1expr <|> op2expr

idstring :: Parser Label
idstring = do
  string "x_"
  n <- many1 digit
  return $ read n

ifexpr :: Parser Expression
ifexpr = do
  string "(if0 "
  e1 <- expr
  char ' '
  e2 <- expr
  char ' '
  e3 <- expr
  char ')'
  return $ IfE0 e1 e2 e3

foldexpr :: Parser Expression
foldexpr = do
  string "(fold "
  e1 <- expr
  char ' '
  e2 <- expr
  char ' '
  l <- lambda2
  char ')'
  return $ FoldE e1 e2 l

op1expr :: Parser Expression
op1expr = do
  char '('
  o <- (const Not <$> (try $ string "not")) <|>
       (const Shl1 <$> (try $ string "shl1")) <|>
       (const Shr16 <$> (try $ string "shr16")) <|>
       (const Shr4 <$> (try $ string "shr4")) <|>
       (const Shr1 <$> (string "shr1"))
  char ' '
  e <- expr
  char ')'
  return $ Op1 o e

op2expr :: Parser Expression
op2expr = do
  char '('
  o <- (const And <$> (try $ string "and")) <|>
       (const Or <$> (try $ string "or")) <|>
       (const Xor <$> (try $ string "xor")) <|>
       (const Plus <$> (string "plus"))
  char ' '
  e1 <- expr
  char ' '
  e2 <- expr
  char ')'
  return $ Op2 o e1 e2

lambda2 :: Parser Lambda2
lambda2 = do
  string "(lambda ("
  s1 <- idstring
  char ' '
  s2 <- idstring
  string ") "
  e <- expr
  char ')'
  return $ Lambda2 s1 s2 e
