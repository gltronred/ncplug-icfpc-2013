{-# LANGUAGE ImplicitParams #-}
-- протяженные диалоги с сервером, вспомогательные функции поверх Networking
module Dialogue where

import Evaluator
import Networking
import Types
import Helpers


import Data.Digest.CRC32
import Control.Applicative
import Control.Concurrent
import Control.Monad
import Data.Function
import Data.List
import Data.Maybe
import System.Random
import Text.Printf
import Data.Time.Clock (getCurrentTime, diffUTCTime)
import System.Timeout (timeout)

num_points = 100 -- количество опрашиваемых перед угадыванием точек

-- получить значение функции в рандомных точках
samplePoints :: Id -> Int -> IO [(Point,Val)]
samplePoints prog_id num = do
  let seed = crc32 prog_id
  let points = getRandomPoints seed num
  vals <- evalId prog_id points
  return $ zip points vals

-- цикл угадывания
guess_loop :: (?verbose :: Int) => Id -> [Program] -> IO Bool
guess_loop prog_id solutions = do
  let sol = head $ take 1 solutions ++ error "no solutions for some reason"
  when (?verbose >= 1) $ printf "Testing solution %s of size %d\n" (show sol) (size sol)
  guess_res <- guess prog_id sol
  case guess_res of
    Left x -> return x
    Right ret@(x, (corr, _)) -> do
      when (?verbose >= 1) $ print ret
      let solutions' = filter (\s -> eval s x == corr) solutions -- добавляем фильтр по точке в цепочку фильтрации
      guess_loop prog_id solutions'

-- угадывание загадки с заданным Id и ограничениями (будь то тест или задание)
solveSample :: (?verbose :: Int, ?solver :: Solver) => Id -> OpConstraint -> IO Bool
solveSample prog_id constraints = do
  startTime <- getCurrentTime
  pairs <- samplePoints prog_id num_points
  let solutions = ?solver constraints pairs
  mres <- timeout 320000000 $ guess_loop prog_id solutions
  stopTime <- getCurrentTime
  when (?verbose >= 1) $ print (diffUTCTime stopTime startTime)
  return $ if isJust mres then fromJust mres else False

-- сабмитит решение задачи, WARNING, можно спалить задания
solveProblem :: (?verbose :: Int, ?solver :: Solver) => Problem -> IO Bool
solveProblem p = solveSample (problemId p) (problemOperators p)

-- ждем некоторое время, пока окно загрузки опустится
sleepUntilWindowReset :: (?verbose :: Int) => Float ->  IO ()
sleepUntilWindowReset min_bound = do
  st <- fromMaybe (error "can't get status") <$> status
  let req_w = statusRequestWindow st
      cpu_w = statusCpuWindow st
      wins = map (\w -> (windowLoad w, windowResetsIn w)) [req_w, cpu_w]
      actual = filter ((min_bound <) . fst) $ reverse $ sortBy (compare `on` snd) wins
  unless (null actual) $ do
    let sleep = snd $ head actual
    when (?verbose >= 2) $ printf "status: %s\n" (show st)
    when (?verbose >= 1) $ printf "sleeping for %f seconds\n" sleep
    threadDelay $ ceiling $ 1000000 * sleep
 where windowLoad w = fromIntegral (windowAmount w) / fromIntegral (windowLimit w)

