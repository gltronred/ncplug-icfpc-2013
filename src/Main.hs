{-# LANGUAGE OverloadedStrings, ImplicitParams #-}
-- Main

import Types
import Networking
import Dialogue
import SimpleSolver

import Network (withSocketsDo)

import qualified Data.ByteString.Char8 as S (unpack)
import Text.Printf
import Data.Function
import Data.List
import Control.Monad

main = withSocketsDo $ let ?verbose = 2 in solveProblems

solveProblems :: (?verbose :: Int) => IO ()
solveProblems = do
  problems <- getProblems
  let unsolved = sortBy (compare `on` problemSize) $ statusIs (==Nothing) problems
  let easy = filter (\p -> let ops = problemOperators p in  ops \\ [Tfold, Fold] == ops) unsolved
  when (?verbose >= 1) $ printf "Got %d problems, %d are failed, %d are solved, %d are unsolved, of which %d are easy\n"
    (length problems) (length $ statusIs (== Just False) problems) (length $ statusIs (== Just True) problems) (length unsolved) (length easy)
  solve_all easy
 where solve_all (p:ps) = do
         when (?verbose >= 1) $ printf "Solving problem %s\n" (show p)
         res <- let ?solver = simpleSolver in solveProblem p
         case res of
           True -> do when (?verbose >= 1) $ printf "Solved!\n"
                      sleepUntilWindowReset 0.7
                      solve_all ps
           False -> do printf "not solved, stopping\n"
       statusIs pred = filter (\p -> pred $ problemSolved p)
