-- всякая всячина, не относящаяся напрямую к задаче
module Helpers where

import System.Random
import Data.Word

-- получение заданного кол-ва точек по seed
getRandomPoints :: Word32 -> Int -> [Word64]
getRandomPoints seed num =
  let gen = mkStdGen $ fromIntegral seed
  in take num $ randoms gen
