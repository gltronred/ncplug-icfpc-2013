{-# LANGUAGE OverloadedStrings #-}

module ExpressionGenerator  where

import Types

import Control.Applicative ((<$>), (<*>))
import Data.List
import qualified Data.Set as S

import Debug.Trace

data OpRestr = Restr [Operator1] [Operator2] Bool Bool Bool deriving Show

newtype VarSet = VarSet Label deriving (Show, Eq)

generate :: Int -> [Operator] -> [Program]
generate n ops = map (Lambda1 cl) $ generate' (n - 1) (VarSet cl) (restrictions ops) True True True True
  where cl = 0

restrictions ops = Restr (filterOp1 ops) (filterOp2 ops) (elem If0 ops) (tfold || elem Fold ops) tfold
  where
    tfold = elem Tfold ops

filterOp1 :: [Operator] -> [Operator1]
filterOp1 ops = map uncoverOp $ filter filterOp' ops
  where
    filterOp' (O1 _) = True
    filterOp' _  = False
    uncoverOp (O1 op) = op
    uncoverOp op = error $ "Error uncovering op1 " ++ show op

filterOp2 :: [Operator] -> [Operator2]
filterOp2 ops = map uncoverOp $ filter filterOp' ops
  where
    filterOp' (O2 _) = True
    filterOp' _  = False
    uncoverOp (O2 op) = op
    uncoverOp op = error $ "Error uncovering op2 " ++ show op

splitNumber2 n = [(n1, n - n1) | n1 <- [1..n-1]]
splitNumber3 n = [(n1, n2, n - n1 - n2) | n1 <- [1..n-1], n2 <- [1..n - n1 - 1]]

newLabel (VarSet ls) = (nl, VarSet nl)
  where
    nl = 1 + ls

generate' n vs@(VarSet ls) restr@(Restr ops1 ops2 ifE foldE tfold) useFold useNot useZero isTop
  | n <= 0 = []
  | n < 5 && useFold && foldE = []
  | n == 1 = addZero useZero $ One: map Id [0..ls]
  | otherwise = otherExpr (not tfold || not isTop) ++ ifExpr ifE ++ foldExpr (useFold && foldE && (tfold == isTop))
    where
      addZero False xs = xs
      addZero True xs = Zero: xs
      (v1, vs1) = newLabel vs
      (v2, vs2) = newLabel vs1
      foldExpr False = []
      foldExpr True = concat [FoldE <$> generate' n1 vs restr False True True False 
                                    <*> generate' n2 vs restr False True True False 
                                    <*> (map (Lambda2 v1 v2) $ generate' n3 vs2 restr False True True False) | 
                             (n1, n2, n3) <- splitNumber3 (n - 2)]
      ifExpr False = []
      ifExpr True = [IfE0 c t f | (n1, n2, n3) <- splitNumber3 (n - 1),
                      c <- filter (\x -> x /= One) $ generate' n1 vs restr useFold False False False,
                      t <- generate' n2 vs restr useFold True True False,
                      f <- generate' n3 vs restr useFold True False False, 
                      t /= f]
      otherExpr False = []
      otherExpr True =  [Op1 op e | op <- filter (\x -> useNot || x /= Not) ops1,
                          e <- generate' (n - 1) vs restr useFold (op /= Not) (op == Not) False]
                        ++
                        [Op2 op e1 e2 | op <- ops2, (n1, n2) <- splitNumber2 (n - 1),
                          e1 <- generate' n1 vs restr useFold True False False,
                          e2 <- generate' n2 vs restr useFold True False False, e1 > e2]


mutateAt :: [Operator] -> Program -> Int -> Int -> Program
mutateAt ops (Lambda1 l e) n r = Lambda1 l $ mutateAt' (restrictions ops) (VarSet l) e n r

infinite [] = error "Infinite []"
infinite xs = concat $ repeat xs

mutateAt' restr vs ifE@(IfE0 c t f) n r
	| n == 0 = ifE
	| n <= s1 = IfE0 (mutateAt' restr vs c (n - 1) r) t f
	| n <= s2 + s1 = IfE0 c (mutateAt' restr vs t (n - s1 - 1) r) f
	| otherwise = IfE0 c t (mutateAt' restr vs f (n - s1 - s2 - 1) r)
	where
		s1 = size c
		s2 = size t
mutateAt' restr vs foldE@(FoldE e1 e2 l@(Lambda2 l1 l2 e3)) n r
	| n == 0 || n == 1 = foldE
	| n <= s1 + 1 = FoldE (mutateAt' restr vs e1 (n - 2) r) e2 l
	| n <= s1 + s2 + 1 = FoldE e1 (mutateAt' restr vs e2 (n - s1 - 2) r) l
	| otherwise = FoldE e1 e2 (Lambda2 l1 l2 (mutateAt' restr (VarSet $ max l1 l2) e3 (n - s1 - s2 - 2) r))
	where
		s1 = size e1
		s2 = size e2
--		tr a = trace (a ++ " - " ++ show n ++ " " ++ show s1 ++ " " ++ show s2)
mutateAt' restr@(Restr _ ops2 _ _ _) vs (Op2 op e1 e2) n r
    | n == 0 = infinite [Op2 op' e1 e2 | op' <- delete op ops2] !! r
	| n <= s = Op2 op (mutateAt' restr vs e1 (n - 1) r) e2
	| otherwise = Op2 op e1 $ mutateAt' restr vs e2 (n - s - 1) r
	where s = size e1
mutateAt' restr@(Restr ops1 _ _ _ _) vs (Op1 op e) 0 r = infinite [Op1 op' e | op' <- delete op ops1] !! r
mutateAt' restr vs (Op1 op e) n r = Op1 op $ mutateAt' restr vs e (n - 1) r
mutateAt' _ (VarSet ls) e 0 r = (infinite xs) !! r
	where
		xs = delete e ([Zero, One] ++ map Id [0..ls])
mutateAt' _ _ e _ _ = e

crossoverAt (Lambda1 l1 e1) (Lambda1 l2 e2) n = (Lambda1 l1 e1', Lambda1 l2 e2')
	where (e1', e2') = crossoverAt' e1 e2 n

crossoverAt' (Op1 op1 e1) (Op1 op2 e2) 0 = (Op1 op1 e2, Op1 op2 e1)
crossoverAt' (Op1 op1 e1) (Op1 op2 e2) n = (Op1 op1 e1', Op1 op2 e2')
	where
		(e1', e2') = crossoverAt' e1 e2 (n - 1)
crossoverAt' (Op2 op1 e11 e12) (Op2 op2 e21 e22) n
	| n == 0 = (Op2 op1 e21 e22, Op2 op2 e11 e12)
	| n <= s = (Op2 op1 e11' e12, Op2 op2 e21' e22)
	| otherwise = (Op2 op1 e11 e12', Op2 op2 e21 e22')
	where
		s = size e11
		(e11', e21') = crossoverAt' e11 e21 (n - 1)
		(e12', e22') = crossoverAt' e12 e22 (n - s - 1)
crossoverAt' e1@(IfE0 c1 t1 f1) e2@(IfE0 c2 t2 f2) n
	| n == 0 = (e2, e1)
	| n <= s1 = (IfE0 c1' t1 f1, IfE0 c2' t2 f2)
	| n <= s1 + s2 = (IfE0 c1 t1' f1, IfE0 c2 t2' f2)
	| otherwise = (IfE0 c1 t1 f1', IfE0 c2 t2 f2')
	where
		s1 = size c1
		s2 = size t1
		(c1', c2') = crossoverAt' c1 c2 (n - 1)
		(t1', t2') = crossoverAt' t1 t2 (n - s1 - 1)
		(f1', f2') = crossoverAt' f1 f2 (n - s1 - s2 - 1)
crossoverAt' f1@(FoldE e11 e12 l1@(Lambda2 l11 l12 e13)) f2@(FoldE e21 e22 l2@(Lambda2 l21 l22 e23)) n
	| n == 0 || n == 1 = (f2, f1)
	| n <= s1 + 1 = (FoldE e11' e12 l1, FoldE e21' e22 l2)
	| n <= s1 + s2 + 1 = (FoldE e11 e12' l1, FoldE e21 e22' l2)
	| otherwise = (FoldE e11 e12 (Lambda2 l11 l12 e13'), FoldE e21 e22 (Lambda2 l21 l22 e23'))
	where
		s1 = size e11
		s2 = size e12
		(e11', e21') = crossoverAt' e11 e21 (n - 2)
		(e12', e22') = crossoverAt' e12 e22 (n - s1 - 2)
		(e13', e23') = crossoverAt' e13 e23 (n - s1 - 2)
crossoverAt' e01 e02 _ = (e02, e01)


