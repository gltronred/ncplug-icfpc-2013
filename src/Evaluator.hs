module Evaluator(eval) where

import Types

import Control.Monad.Reader
import Data.Bits
import qualified Data.IntMap.Strict as M
import Control.Applicative

type Env = M.IntMap Point
type Evaluator = Reader Env

eval :: Program -> Point -> Val
eval (Lambda1 var expr) x = runEvaluator (evalM expr) $ M.singleton var x

runEvaluator = runReader
lookup_env = M.lookup -- сейчас, пока имена не уникальные, приходится работать со стеком. В дальнейшем лучше будет переехать на Data.Map

-- Эвалуатор выражения
evalM :: Expression -> Evaluator Val
evalM Zero = return 0
evalM One = return 1
evalM (Id var) = do
  mval <- asks (lookup_env var)
  case mval of
    Nothing -> error $ "no variable x_" ++ show var
    Just val -> return val
evalM (Op1 op exp) = let
  f = case op of
    Not -> complement
    Shl1 -> shift `flip` 1
    Shr1 -> shift `flip` (-1)
    Shr4 -> shift `flip` (-4)
    Shr16 -> shift `flip` (-16)
  in f <$> evalM exp
evalM (Op2 op2 exp1 exp2) = let
  f = case op2 of
    And -> (.&.)
    Or -> (.|.)
    Xor -> xor
    Plus -> (+)
  in f <$> evalM exp1 <*> evalM exp2
evalM (IfE0 test exp1 exp2) = do
  res <- evalM test
  if res == 0 then evalM exp1
    else evalM exp2
evalM (FoldE exp1 exp2 (Lambda2 var1 var2 inner_exp)) = do
  v1 <- evalM exp1
  v2 <- evalM exp2
  do_fold 8 v1 v2
 where do_fold 0 v acc = return acc
       do_fold n v acc = do
         let b = v .&. 0xFF
         res <- local (\env -> M.insert var1 b $ M.insert var2 acc env) $ evalM inner_exp
         do_fold (n-1) (v `shift` (-8)) res
