{-# LANGUAGE OverloadedStrings #-}

module Networking (getProblems, evalId, evalProgram, guess, train, status) where

import Types

import Control.Applicative ((<$>), (<*>))
import Control.Concurrent (threadDelay)
import Control.Monad (mzero)
import Data.Aeson
import Data.Bits
import qualified Data.ByteString.Char8 as S
import Data.ByteString.Lazy (toStrict)
import Data.ByteString.Lazy.Builder
import Data.Char (chr,ord)
import qualified Data.Map as M
import Data.Maybe
import Data.Monoid (Monoid(..),mappend)
import Data.Word (Word64(..))
import Network.Socket
import Network.HTTP.Conduit
import Network.HTTP.Types (statusCode)

url = "http://icfpc2013.cloudapp.net/"
secretToken = "03414fmJjlciWRYRByL6Yzc0HznWyzWebboi3XVy"
additionalToken = "vpsH1H"
authParam = "?auth=" ++ secretToken ++ additionalToken

instance FromJSON Problem where
  parseJSON (Object v) = Problem <$>
                         v .: "id" <*>
                         v .: "size" <*>
                         v .: "operators" <*>
                         v .:? "solved" <*>
                         v .:? "timeLeft"
  parseJSON _ = mzero

getProblems :: IO [Problem]
getProblems = generalNetwork "myproblems" Nothing [] [] $ \resp -> do
  -- print $ (eitherDecode resp :: Either String [Problem])
  return resp

infixr 4 <>
(<>) :: Monoid m => m -> m -> m
(<>) = mappend

convertToHex :: Word64 -> S.ByteString
-- convertToHex w = toStrict $ toLazyByteString $ byteString "0x" <> P.primFixed P.word64HexFixed w
convertToHex w = let
  toHex a | 0<=a && a<=9 = chr $ a + ord '0'
          | 10<=a&&a<=15 = chr $ a-10 + ord 'A'
  digit w i = fromIntegral $ (w .&. (0xF `shiftL` (i*4))) `shiftR` (i*4)
  in "0x" `S.append` S.pack (map (toHex . digit w) $ reverse [0..15])

convertToWord :: S.ByteString -> Word64
convertToWord b = S.foldl' (\a x -> a*16+toDec x) (0::Word64) $ S.drop 2 b
  where toDec c | '0'<=c && c<='9' = fromIntegral $ ord c - ord '0'
        toDec c | 'a'<=c && c<='f' = fromIntegral $ ord c - ord 'a' + 10
        toDec c | 'A'<=c && c<='F' = fromIntegral $ ord c - ord 'A' + 10
        toDec c | otherwise = 0

data EvalResponseHelper = EvalRespHelper { erhStatus :: S.ByteString
                                         , erhOutputs :: Maybe [S.ByteString]
                                         , erhMessage :: Maybe S.ByteString } deriving (Eq,Show)
instance FromJSON EvalResponseHelper where
  parseJSON (Object v) = EvalRespHelper <$>
                         v .: "status" <*>
                         v .:? "outputs" <*>
                         v .:? "message"
  parseJSON _ = mzero

-- points may be any length
evalId :: Id -> [Point] -> IO [Val]
evalId i points = generalEval (Just i) Nothing points

evalProgram :: Program -> [Point] -> IO [Val]
evalProgram p points = generalEval Nothing (Just p) points

generalEval :: Maybe Id -> Maybe Program -> [Point] -> IO [Val]
generalEval _ _ [] = return []
generalEval mid mprog points = let
  (ps, other) = splitAt 256 points -- up to 256 points, if more then split
  in generalNetwork "eval" (Just $ object [ if isJust mid then "id" .= fromJust mid else "program" .= fromJust mprog
                                          , "arguments" .= map convertToHex points]) [] [] $ \resp -> do
    if erhStatus resp == "error"
      then error $ S.unpack $ fromJust $ erhMessage resp
      else do
        otherRes <- generalEval mid mprog other
        let theseRes = map convertToWord $ fromJust $ erhOutputs resp
        return $ theseRes ++ otherRes

data GuessResponse = GuessResponse { guessStatus :: S.ByteString
                                   , guessValues :: Maybe [S.ByteString]
                                   , guessMessage :: Maybe S.ByteString } deriving (Eq,Show)
instance FromJSON GuessResponse where
  parseJSON (Object v) = GuessResponse <$>
                         v .: "status" <*>
                         v .:? "values" <*>
                         v .:? "message"

{-
guess returns:
* Left True -- function guessed correctly
* Left False -- problem is left unsolved
* Right (x,(y0,y1)) -- y0 = corr(x), y1 = our(x)
-}
guess :: Id -> Program -> IO (Either Bool (Point,(Point,Point)))
guess id prog = generalNetwork "guess" (Just $ object [ "id" .= id, "program" .= prog]) (Left False) (Left True) $ \resp -> do
  case guessStatus resp of
    "error" -> error $ S.unpack $ fromJust $ guessMessage resp
    "win" -> return $ Left True
    "mismatch" -> do
      return $ Right $ (\[a,b,c] -> (a,(b,c))) $ map convertToWord $ fromJust $ guessValues resp

instance FromJSON Training where
  parseJSON (Object v) = Training <$>
                         v .: "challenge" <*>
                         v .: "id" <*>
                         v .: "size" <*>
                         v .: "operators"
  parseJSON _ = mzero

{- Spec says that ops can be: [], [Tfold], [Fold] -}
train :: Int -> [Operator] -> IO (Maybe Training)
train size ops = generalNetwork "train" (Just $ object ["size".=size, "operators".=ops]) Nothing Nothing $ return . Just

instance FromJSON Window where
  parseJSON (Object v) = Window <$>
                         v .: "resetsIn" <*>
                         v .: "amount" <*>
                         v .: "limit"
  parseJSON _ = mzero

instance FromJSON Status where
  parseJSON (Object v) = Status <$>
                         v .: "easyChairId" <*>
                         v .: "contestScore" <*>
                         v .: "lightningScore" <*>
                         v .: "trainingScore" <*>
                         v .: "mismatches" <*>
                         v .: "numRequests" <*>
                         v .: "requestWindow" <*>
                         v .: "cpuWindow" <*>
                         v .: "cpuTotalTime"
  parseJSON _ = mzero

status :: IO (Maybe Status)
status = generalNetwork "status" Nothing Nothing Nothing $ return . Just

generalNetwork :: (FromJSON r) => String -> Maybe Value -> a -> a -> (r -> IO a) -> IO a
generalNetwork path obj r410 r412 parseResponse = withSocketsDo $ do
  req' <- parseUrl $ url ++ path ++ authParam
  let reqBody = encode $ fromJust obj
      req'' = if isJust obj
              then req' { requestBody = RequestBodyLBS reqBody }
              else req'
      req = req'' { method = "POST", checkStatus = \_ _ _ -> Nothing, responseTimeout = Just 20000000 }
  result <- withManager $ httpLbs req
  case statusCode $ responseStatus result of
    400 -> error "Wrong eval input"
    401 -> error "Auth required"
    404 -> error "Wrong challenge"
    410 -> return r410 -- problem requested more than 5 mins ago
    412 -> return r412 -- problem already solved
    413 -> error "Too big request"
    429 -> threadDelay 50000 >> generalNetwork path obj r410 r412 parseResponse
    200 -> parseResponse $ fromJust $ decode $ responseBody result

