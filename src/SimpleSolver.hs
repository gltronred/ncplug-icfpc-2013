
module SimpleSolver where

import Evaluator
import ExpressionGenerator
import Types

-- Тупой переборный поиск функций
simpleSolver :: Solver
simpleSolver ops pairs = let progs = concatMap (generate `flip` ops) [1..]
                          in filter (\prog -> and $ map (\(point, val) -> eval prog point == val) pairs) progs


