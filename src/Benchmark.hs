{-# LANGUAGE BangPatterns #-}
module Main where

import ExpressionGenerator
import Types
import Helpers
import Evaluator

import System.Environment
import Data.Time.Clock (getCurrentTime, diffUTCTime)
import Text.Printf

import Data.Word

all_ops = map O1 [minBound..maxBound] ++ map O2 [minBound..maxBound] ++ [If0] -- , Tfold, Fold]

main = do
  [a1,a2,a3] <- getArgs
  let size = read a1 :: Int
      num_points = read a2 :: Int
      seed = read a3 :: Word32
  printf "Testing with programs of size %d on number of points %d\n" size num_points
  t1 <- getCurrentTime
  let progs = generate size all_ops
  let points = getRandomPoints seed num_points
  let (count, sign) = count_sum (0 :: Int) 0 $ map (\p -> eval_prog p points) progs
  sign `seq` printf "Generated %d programs of length %d\n" count size
  t2 <- getCurrentTime
  let diff = diffUTCTime t2 t1
  printf "In %s\n" (show diff)
  printf "Avg program processing time: %s" (show $ diff / fromIntegral count)

eval_prog :: Program -> [Point] -> Val
eval_prog p xs = sum $ map (eval p) xs

count_sum !c !s [] = (c, s)
count_sum !c !s (x:xs) = count_sum (c+1) (s + x) xs
