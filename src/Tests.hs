{-# LANGUAGE ImplicitParams #-}
module Tests where

import Dialogue
import Evaluator
import ExpressionGenerator
import GenAlg
import Helpers
import Networking
import Parser
import SimpleSolver
import Types

import Control.Monad

import Text.Printf

import qualified Data.ByteString.Char8 as C

-- получить тестовый пример заданных размера и сложности на опыты
grabTestSample :: Int -> OpConstraint -> IO (Program, Training)
grabTestSample size ops = do
  Just sample <- train size ops
  let prog_str = C.unpack $ trainingChallenge sample
      prog = case loadProg prog_str of
        Left err -> error $ "can't parse:" ++ show err
        Right x -> x
  return $ (prog, sample)

-- Тестирование Evaluator на соответствие серверу
do_evaluator_test :: (?verbose :: Int) => Int -> OpConstraint -> IO (Either (Program, [(Point,Val)]) ())
do_evaluator_test size ops = do
  (prog, sample) <- grabTestSample size ops
  pairs <- samplePoints (trainingId sample) num_points
  let res = and $ map (\(p,v) -> eval prog p == v) pairs

  when (?verbose >= 1 || not res) $ do
    putStrLn $ "testing: " ++ show prog
    putStrLn $ "in points: " ++ show pairs
  case res of
    False -> do putStrLn "Error!"
                return $ Left (prog, pairs)
    True -> do when (?verbose >= 1) $ putStrLn "Success!"
               return $ Right ()


-- Тестирование решалки на training примерах
do_simple_solver_test :: (?verbose :: Int) => Int -> OpConstraint -> IO Bool
do_simple_solver_test size constraints = do
  (prog, sample) <- grabTestSample size constraints
  when (?verbose >= 1) $ printf "Server thought of %s of size %d\n" (show prog) (trainingSize sample)
  let ?solver = simpleSolver
  solveSample (trainingId sample) (trainingOperators sample)

-- сабмитит решение задачи, WARNING, можно спалить задания
solveProblem :: (?verbose :: Int) => Problem -> IO Bool
solveProblem p = let ?solver = simpleSolver in solveSample (problemId p) (problemOperators p)

--
gaSolver :: Int -> Solver
gaSolver size ops pairs = [p]
   where 
     (p, v, s, i) = gaExampleRun (map fst pairs) (map snd pairs) size ops False 100

-- сабмитит решение задачи, WARNING, можно спалить задания
gaSolveProblem :: (?verbose :: Int) => Problem -> IO Bool
gaSolveProblem p = let ?solver = gaSolver (problemSize p) in solveSample (problemId p) (problemOperators p)

-- Тестирование решалки на training примерах
-- аргументы: размер задачи, ограничения([],[Fold],[Tfold]), количество точек оценки, количество итераций ГА
ga_training_test :: Int -> OpConstraint -> Int -> Int -> IO Bool
ga_training_test size constraints num_args maxIter = do
  (prog, sample) <- grabTestSample size constraints
  let prog_id = trainingId sample
  let operators = trainingOperators sample
  printf "Server thought of %s of size %d.\n Operators %s\n" (show prog) (trainingSize sample) (show operators)
  pairs <- samplePoints prog_id num_args
  let ga_gen prs = gaExampleRun (map fst prs) (map snd prs) (trainingSize sample) operators (Bonus `elem` operators) maxIter
  ga_loop prog_id ga_gen pairs
  where ga_loop prog_id gen pairs = do
         printf "Running ga on %d points\n" (length pairs)
         let (best, _, solved, i) = gen pairs
         case solved of
           True -> do printf "Found this: %s of size %s" (show best) (show (Types.size best))
                      let correct = and $ map (\(point, val) -> eval best point == val) pairs
                      when (not correct) $ printf "GA returned incorrect solution"
                      resp <- guess prog_id best
                      case resp of
                        Left x -> return x
                        Right ret@(x, (corr, _)) -> do
                          print ret
                          ga_loop prog_id gen ((x,corr):pairs)
           False -> printf "cant solve(maxIter exceeded)\n"
