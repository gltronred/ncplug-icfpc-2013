{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE ImplicitParams #-}

module GenAlg where

import Types
import ExpressionGenerator
import Evaluator

import Control.Applicative ((<$>), (<*>))
import qualified Data.Sequence as S
import qualified Data.List as L
import qualified Data.Foldable as F
import System.Random
import Data.Bits
import Data.Word

import Debug.Trace

myTrace x y = y -- trace x y

data Seed = Seed [Int]

instance Show Seed where
  show _ = "*"

o1num = 1 + fromEnum (maxBound :: Operator1) - fromEnum (minBound :: Operator1)
o1ex = enumFromTo (minBound :: Operator1) (maxBound :: Operator1)

o2num = 1 + fromEnum (maxBound :: Operator2) - fromEnum (minBound :: Operator2)
o2ex = enumFromTo (minBound :: Operator2) (maxBound :: Operator2)

opnum = o1num + o2num
ifnum = opnum + 1
flnum = ifnum + 1
maxnum = flnum + 1

rootVar  = VarSet 1

generateUsualFromSeed 1 _ _ _ (Seed (0:s)) = (Zero, [0], Seed s)
generateUsualFromSeed 1 _ _ _ (Seed (1:s)) = (One, [1], Seed s)
generateUsualFromSeed 1 vs@(VarSet m) r isTop (Seed (l:s)) 
  | l - 2 < m = (Id (l - 2), [l], Seed s)
  | otherwise = generateUsualFromSeed 1 vs r isTop $ Seed (l `mod` (m + 2):s)

generateUsualFromSeed n vs@(VarSet v) r@(Restr ops1 ops2 ifE foldE tfold) isTop (Seed s@(l:ss@(l':ss'@(l'':ss''))))
  | n < 2 = error $ "Low n: " ++ show n ++ ", " ++ show vs ++ ", " ++ show r ++ ", " ++ show isTop ++ ", " ++ show (take 1 s)
  | foldE && tfold && isTop && n >= 5 && l /= flnum = generateUsualFromSeed n vs r isTop $ Seed (flnum: ss)
  | l <= o1num || n == 2 = let (res, c, s') = generateUsualFromSeed (n - 1) vs r False (Seed ss) in (Op1 (o1ex !! (l `mod` o1num)) res, l:c, s')
  | (l <= opnum && n >= 3) || n == 3 = 
    let n1 = 1 + l' `mod` (n - 2)
        (res1, c', s') = generateUsualFromSeed n1 vs r False $ Seed ss' 
        (res2, c'', s'') = generateUsualFromSeed (n - n1 - 1) vs r False s' 
    in (Op2 (o2ex !! ((l - o1num) `mod` o2num)) res1 res2, l:l':c' ++ c'', s'')
  | ifE && l == ifnum && n >= 4 =  
    let n1 = 1 + l' `mod` (n - 3)
        n2 = 1 + l'' `mod` (n - n1 - 2)
        (c, c', s') = generateUsualFromSeed n1 vs r False $ Seed ss''
        (t, c'', s'') = generateUsualFromSeed n2 vs r False s' 
        (f, c''', s''') = generateUsualFromSeed (n - n1 - n2 - 1) vs r False s'' 
    in (IfE0 c t f, l:l':l'':c' ++ c'' ++ c''', s''')
  | foldE && l == flnum && n >= 5 = 
    let n1 = 1 + l' `mod` (n - 4)
        n2 = 1 + l'' `mod` (n - n1 - 3)
        (e1, c', s') = generateUsualFromSeed n1 vs foldRestr False $ Seed ss''
        (e2, c'', s'') = generateUsualFromSeed n2 vs foldRestr False s' 
        (v', vs') = newLabel vs
        (e3, c''', s''') = generateUsualFromSeed (n - n1 - n2 - 2) vs' foldRestr False s'' 
    in (FoldE e1 e2 (Lambda2 v v' e3), l:l':l'':c' ++ c'' ++ c''', s''') 
  | otherwise = generateUsualFromSeed n vs r isTop $ Seed (l - 1: ss)
  where
   foldRestr = Restr ops1 ops2 ifE False False

generateUsualFromSeed n vs r isTop (Seed s) = error $ "Extra generation: " ++ show n ++ ", " ++ show vs ++ ", " ++ show r ++ ", " ++ show isTop ++ ", " ++ show (take 1 s)

generateBonusFromSeed n vs r _ (Seed s@(l:ss@(l':ss'@(l'':ss''@(l''':ss'''))))) = (IfE0 (Op2 And e1 e2) t f, l:l':l'':c' ++ c'' ++ c''' ++ c'''', s'''')
    where
        n1 = 1 + l' `mod` (n - 5)
        n2 = 1 + l'' `mod` (n - n1 - 4)
        n3 = 1 + l''' `mod` (n - n1 - n2 - 3)
        n4 = n - n1 - n2 - n3 - 2
        (e1, c', s') = generateUsualFromSeed n1 vs r False $ Seed ss'''
        (e2, c'', s'') = generateUsualFromSeed n2 vs r False s' 
        (t, c''', s''') = generateUsualFromSeed n3 vs r False s'' 
        (f, c'''', s'''') = generateUsualFromSeed n4 vs r False s''' 

generateFromSeed False = generateUsualFromSeed
generateFromSeed True = generateBonusFromSeed

data ProgramData = ProgramData Program [Int] Int (Maybe Integer) deriving (Eq,Show) 

type Population = S.Seq ProgramData

data Generation rg = Generation Int Int Population Bool Seed deriving (Show)

seed :: Seed 
seed = Seed $ randomRs (0, maxnum) (mkStdGen 0)

initialGeneration s r popSize isBonus is = myTrace ("Initial: " ++ show population) $ Generation 0 popSize population False is'
    where
       (population, is') = randomPrograms s r popSize isBonus is

randomPrograms size r popSize isBonus is = (S.fromList $ map toData ps, is')
    where
       makeProgram s = generateFromSeed isBonus size rootVar r True s
       iteration (ps, n, s) = let (p, c, s') = makeProgram s in ((p, c):ps, n + 1, s')
       notEnough (_, n, _) = n <= popSize
       (ps, _, is') = head $ dropWhile notEnough $ iterate iteration ([], 0, is)
       toData (p, c) = ProgramData (Lambda1 0 p) c (length c) Nothing

score pts vals (ProgramData p c l Nothing) = ProgramData p c l (Just $ sum difs)
    where difs = zipWith metrics (map (eval p) pts) vals
score _ _ pd = pd

metrics x1 x2 = abs (toInteger x1 - toInteger x2)

getFittest pts vals n pds = (sol, solved)
   where 
       sol = S.take n $ S.unstableSortBy greaterProgram $ fmap (score pts vals) pds
       solved = case S.viewl sol of
                    (ProgramData p _ _ (Just 0)) S.:< _ -> trace ("Found solution " ++ show p) $ True
                    otherwise -> False

greaterProgram (ProgramData _ _ _ (Just v1)) (ProgramData _ _ _ (Just v2)) = compare v1 v2

insertAt _ _ [] = []
insertAt 0 y (x:xs) = y: xs
insertAt n y (x:xs) = x: insertAt (n - 1) y xs

bonusN False n = n
bonusN True 0 = 5
bonusN True 4 = 6
bonusN True n = n

mutation isBonus r n a (ProgramData p c l _) = myTrace ("Mutation at " ++ show n' ++ " -> " ++ show a ++ " for " ++ show c ++ "->" ++ show newCode ++ "/" ++ show c' ++ " " ++ show (size p) ++ "/" ++ show (size p')) $ ProgramData (Lambda1 0 p') c' (length c') Nothing
    where
        n' = (bonusN isBonus n) `mod` l
        newCode = insertAt n' a c
        (p', c', _) = generateFromSeed isBonus (size p - 1) rootVar r True $ Seed $ infinite newCode

glue _ _ [] = []
glue 0 _ ys = ys
glue n (x:xs) (y:ys) = x: glue (n - 1) xs ys

crossover isBonus r n (ProgramData p1 c1 l1 _ ) (ProgramData _ c2 l2 _) = myTrace ("Crossover at " ++ show n' ++ " for " ++ show c1 ++ " + " ++ show c2 ++ "->" ++ show newCode ++ "/" ++ show c) $ ProgramData (Lambda1 0 p) c (length c) Nothing
    where
        n' = n `mod` l1
        newCode = glue n' c1 c2
        (p, c, _) = generateFromSeed isBonus (size p1 - 1) rootVar r True $ Seed $ infinite newCode

gaCycle pts vals size r isBonus nOfMut nOfCross nOfFittest (Generation i popSize pds _ (Seed is)) = trace ("Iteration " ++ show i ++ " -> " ++ show (S.index fittestPds 0)) $ Generation (i + 1) popSize newPds solved is''''
    where
        (fittestPds, solved) = getFittest pts vals nOfFittest pds
        (cs, is') = splitAt nOfCross is
        (m1s, is'') = splitAt nOfMut is'
        (m2s, is''') = splitAt nOfMut is''
        mutatedPds = S.zipWith ($) (S.fromList $ zipWith ($) (map (mutation isBonus r) m1s) m2s) (S.take nOfMut fittestPds)
        crossovPds = S.zipWith ($) (S.zipWith ($) (S.fromList $ map (crossover isBonus r) cs) fittestPds) (S.take nOfCross $ S.reverse fittestPds)
        (randomPds, is'''') = randomPrograms size r (popSize - nOfMut - nOfCross - nOfFittest ) isBonus $ Seed is'''
        newPds = fittestPds S.>< randomPds S.>< mutatedPds S.>< crossovPds

genalg pts vals size r isBonus popSize nOfMut nOfCross nOfFittest maxIter rg = getSol $ head $ dropWhile notSolved $ iterate (gaCycle pts vals size r isBonus nOfMut nOfCross nOfFittest) initialPop
    where 
      initialPop = initialGeneration size r popSize isBonus seed
      notSolved (Generation i _ _ a _) = not a && i < maxIter
      getSol (Generation i _ pds a _) = (S.index pds 0, a, i)

{-
pts - точки
vals - значения в этих точках
size - размер выражения
maxIter - максимальное количество итераций
на выходе - (решение, точное ли оно, количество итераций)
-}
gaExampleRun pts vals size r False maxIter = (p, v, solved, i)
    where ((ProgramData p _ _ v), solved, i) = genalg pts vals (size - 1) (restrictions r) False 3000 1000 10 1000 maxIter (mkStdGen 0)

gaExampleRun pts vals size r True maxIter = (p, v, solved, i)
    where ((ProgramData p _ _ v), solved, i) = genalg pts vals (size - 1) (restrictions r) True 300 100 10 100 maxIter (mkStdGen 0)

fExpr = FoldE (Id 0) One (Lambda2 1 2 (Op2 Xor (Id 1) (Id 2)))
expr1 = Op2 And (Id 0) (Op1 Shl1 (Op1 Shl1 One))    
expr2 = Op2 Plus (Id 0) (Op2 Plus (Id 0) (Op2 Plus One (Op1 Shl1 One)))
expr3 = IfE0 (Op2 And (Id 0) (Op1 Shl1 One)) expr2 expr1
program = Lambda1 0 expr3
pts = [0,37..300]
vals = map (eval program) pts
psize = size program
test maxIter = (sum $ zipWith (-) vals $ map (eval p) pts, v, s, i, p)
    where (p, v, s, i) = gaExampleRun pts vals psize [] True maxIter

--main = print $ test 1000